package id.ihwan.wisataque.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.ihwan.wisataque.R
import id.ihwan.wisataque.data.Blog
import kotlinx.android.synthetic.main.item_blog.view.*

class BlogAdapter(val data: ArrayList<Blog>, val listener: (Blog) -> Unit) :
    RecyclerView.Adapter<BlogAdapter.BlogViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BlogViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_blog, parent, false)
        return BlogViewHolder(view)
    }

    override fun getItemCount(): Int = data.size


    override fun onBindViewHolder(holder: BlogViewHolder, position: Int) {
        holder.bindItem(data[position], listener)

    }

    class BlogViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindItem(data: Blog, listener: (Blog) -> Unit) {
            itemView.titleBlog.text = data.title
//            Glide.with(itemView.context).load(data.image).into(itemView.gambar)
            itemView.setOnClickListener { listener(data) }
        }

    }
}