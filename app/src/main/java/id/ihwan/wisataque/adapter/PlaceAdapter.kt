package id.ihwan.wisataque.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import id.ihwan.wisataque.R
import id.ihwan.wisataque.data.Place
import kotlinx.android.synthetic.main.item_places.view.*

class PlaceAdapter(val data: ArrayList<Place>, val listener: (Place) -> Unit) :
    RecyclerView.Adapter<PlaceAdapter.PlacesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlacesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_places, parent, false)
        return PlacesViewHolder(view)
    }

    override fun getItemCount(): Int = data.size


    override fun onBindViewHolder(holder: PlacesViewHolder, position: Int) {
        holder.bindItem(data[position], listener)


    }

    class PlacesViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bindItem(data: Place, listener: (Place) -> Unit) {
            itemView.nama.text = data.nama
            Glide.with(itemView.context).load(data.gambar).into(itemView.gambar)
            itemView.setOnClickListener {
                listener(data)
            }
        }

    }
}