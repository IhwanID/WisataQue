package id.ihwan.wisataque.feature.main.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import id.ihwan.wisataque.R
import id.ihwan.wisataque.adapter.BlogAdapter
import id.ihwan.wisataque.adapter.PlaceAdapter
import id.ihwan.wisataque.data.Blog
import id.ihwan.wisataque.data.Place
import id.ihwan.wisataque.feature.DetailActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity

class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView.layoutManager = GridLayoutManager(applicationContext, 3)
        recyclerView.setHasFixedSize(true)

        recyclerViewBlog.layoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)


        val database = FirebaseDatabase.getInstance().getReference("places")
        val blogDatabase = FirebaseDatabase.getInstance().getReference("blog")

        database.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {

                val placeList = ArrayList<Place>()
                for(ds in dataSnapshot.children){
                    val  places = ds.getValue(Place::class.java) as Place
                    placeList.add(places)
                }

                val adapter = PlaceAdapter(placeList) {
                    startActivity<DetailActivity>("title" to it.nama, "image" to it.gambar, "detail" to it.detail)
                }

                recyclerView.adapter = adapter
                adapter.notifyDataSetChanged()

            }

        })

        blogDatabase.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {

                val blogList = ArrayList<Blog>()
                for(ds in dataSnapshot.children){
                    val  blogs = ds.getValue(Blog::class.java) as Blog
                    blogList.add(blogs)
                }

                val adapter = BlogAdapter(blogList) {
                    startActivity(Intent(this@MainActivity, DetailActivity::class.java))
                }

                recyclerViewBlog.adapter = adapter
                adapter.notifyDataSetChanged()

            }

        })


    }



}

