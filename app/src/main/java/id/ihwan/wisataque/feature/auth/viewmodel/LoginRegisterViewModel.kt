package id.ihwan.wisataque.feature.auth.viewmodel

import android.util.Log
import androidx.databinding.ObservableField
import com.google.firebase.auth.FirebaseAuth
import id.ihwan.wisataque.base.BaseViewModel

class LoginRegisterViewModel : BaseViewModel() {

    val auth = FirebaseAuth.getInstance()

    val emailField = ObservableField<String>("")
    val passwordField = ObservableField<String>("")
    val buttonSubmitField = ObservableField("Register")

    val email get() = emailField.get() ?: ""
    val password get() = passwordField.get() ?: ""

    fun doRegister() {
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (it.isSuccessful) {
                    Log.d("REGISTER :", "registered")
                } else {
                    Log.d("REGISTER :", "failed")
                }
            }
    }


}