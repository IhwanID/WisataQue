package id.ihwan.wisataque.feature.auth.view

import android.os.Bundle
import id.ihwan.wisataque.R
import id.ihwan.wisataque.base.BaseActivity
import id.ihwan.wisataque.databinding.ActivityLoginRegisterBinding
import id.ihwan.wisataque.feature.auth.viewmodel.LoginRegisterViewModel

class LoginRegisterActivity : BaseActivity<ActivityLoginRegisterBinding, LoginRegisterViewModel>(LoginRegisterViewModel::class.java) {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        bindLayout(R.layout.activity_login_register)
        binding.viewModel = this@LoginRegisterActivity.viewModel


    }

}
