package id.ihwan.wisataque.feature

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.ihwan.wisataque.R
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val  nama = intent.getStringExtra("title")
        val  gambar = intent.getStringExtra("image")
        val  detail = intent.getStringExtra("detail")

        titlePlace.text = nama
        descriptionPlace.text = detail

    }
}
