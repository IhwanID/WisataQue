package id.ihwan.wisataque.feature

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import id.ihwan.wisataque.R
import id.ihwan.wisataque.feature.auth.view.LoginRegisterActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val r = Runnable {
            startActivity(Intent(this, LoginRegisterActivity::class.java))
        }

        Handler().postDelayed(r, 1500)
    }
}
