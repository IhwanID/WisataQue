package id.ihwan.wisataque.data

data class Place(
        val nama : String? = "",
        val detail : String? = "",
        val alamat : String? = "",
        val gambar : String? = "",
        val latitude : String? = "",
        val longitude : String? = "",
        val tiket : String? = ""
)