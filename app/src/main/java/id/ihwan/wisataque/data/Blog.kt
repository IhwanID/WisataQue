package id.ihwan.wisataque.data

data class Blog(
    val title : String? = "",
    val body : String? = "",
    val image : String? = ""
)