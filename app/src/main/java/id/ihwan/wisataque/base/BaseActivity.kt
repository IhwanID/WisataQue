package id.ihwan.wisataque.base

import android.view.View
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders

abstract class BaseActivity<DB : ViewDataBinding, VM : ViewModel>(private val viewModelClass: Class<VM>) : AppCompatActivity() {

    protected lateinit var binding: DB
    protected lateinit var viewModel: VM



    protected fun bindLayout(layout: Int){
        binding = DataBindingUtil.setContentView(this, layout)
    }

    protected fun bindViewFullscreen(layout: Int) {
        // set fullscreen
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        val decorView = window.decorView
        decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        binding = DataBindingUtil.setContentView(this, layout)
    }

    protected fun initViewModel(){
        viewModel = ViewModelProviders.of(this).get(viewModelClass)
    }



}